# Webhooks design overview

## Purpose

Kernel Workflow activity in Gitlab and Jira generate webhook events which are consumed by instances
of the various modules ("webhooks") in the webhooks folder. Some webhooks also consume events
from the internal UMB (Universal Message Bus) and events generates by other webhooks.

## Operation

Each webhook begins with an instance of the `session.SessionRunner` class. The `SessionRunner`
provides connections to the Gitlab REST and GraphQL APIs, Jira's REST API, etc. It also provides an
instance of the `rh_metadata.Projects` class which holds metadata about each webhook loaded from
`rh_metadata.yaml`.

The `SessionRunner` processes event messages. This can be a single event provided on the command
line in a json file, or a "dummy" event generated on the fly to match an MR URL given on the
command line. Or, with the right rabbitmq service information, the `SessionRunner` will connect
to the rabbitmq server and continuously process incoming events.

## Event Processing

For each event, the `SessionRunner` decides whether to ignore the event or pass it on to the
webhook's registered handler function. The event headers and payload are loaded into a
`session_events.BaseEvent` object which derives commonly used properties and includes a
`should_be_processed` method which is a boolean indicating whether the event should be ignored
or processed for the given environment and webhook.

When `should_be_processed` is called, the `BaseEvent` runs the set of filter functions registered
for the given event type. If any of the filter functions return False, the event is ignored.
If the event passes the filter functions, then it is put through the set of registered trigger
functions. If any trigger function returns True, the event is passed on to the webhook's
registered handler. If none of the trigger functions return True, the event is ignored.

Note a webhook's metadata may set `skip_session_filter_check` in which case no filter functions
are called by `should_be_processed`. Further, a webhook's metadata may set
`skip_session_trigger_check` to skip any trigger functions. In the case were
`skip_session_filter_check` is True or there are no registered triggers for the event then
`should_be_processed` returns True.

## BaseEvent Types

Currently there are the following subclasses of the `BaseEvent` class:

- `GitlabEvent`
- `GitlabMREvent`
- `GitlabNoteEvent`
- `JiraEvent`
- `UmbBridgeEvent`
- `AmqpEvent`

### BaseEvent Filters & Triggers

`BaseEvent` and any subclass always include these filters:

- `filter_environment`: if the event can be associated with an `rh_metadata.Project`, then
ensure the webhook's environment matches the Project. For production instances of the webhooks,
this filters out events associated with staging projects and for staging instances of the hooks
it filters out production project events.
- `filter_bots`: if the event is determined to be from a known bot account then it is filtered out.

`BaseEvent` does not have any triggers.

### GitlabEvent Filters & Triggers

`GitlabEvent` or a subclass is used for any event whose header `message-type` is `gitlab`.

It and any subclass always include these filters:

- `filter_drafts`: requires the webhook to not have `run_on_drafts` set. If it is not set and the
MR is determined to be a draft then ignore the message, except in the case the event indicates a
change in the draft status.
- `filter_status`: if the event indicates an MR which is closed or merged then ignore the message,
except in the case the event indicates the MR is closing.

`GitlabEvent` and any subclass always include these triggers:

- `has_missing_label`: if the event indicates the MR does not have all of the expected
`label_prefixes` for the webhook then this trigger returns True and the event should be passed to
the handler.
- `has_blocked_label_mismatch`: requires the webhook to have `run_on_blocking_mismatch` set. If it
is set and the event indicates the MR's current Blocking label does not match the MR's blocked
discussions state then trigger the webhook.

### GitlabMREvent Filters & Triggers

`GitalbMREvent` is used for any Gitlab event whose `object_kind` is `merge_request`.

`GitlabMREvent` does not have any extra filters beyond what `GitlabEvent` provides.

`GitlabMREvent` includes these triggers:

- `has_open_action`: if the event indicates an MR which has just opened or been reopened then
trigger the webhook.
- `has_changed_label`: if the event indicates any of the webhook's `label_prefixes` labels on the
MR have changed or been added or removed then trigger the webhook.
- `has_changed_commits`: requires the webhook to have `run_on_changed_commits` set. If it
is set and the event indicates commits have changed then trigger the webhook.
- `has_changed_description`: requires the webhook to have `run_on_changed_description` set.
If it is set and the event indicates the MR description has changed then trigger the webhook.
- `has_changed_to_draft`: requires the webhook to have `run_on_changed_to_draft` set. If it
is set and the event shows the MR entered draft state then trigger the webhook.
- `has_changed_to_ready`: requires the webhook to have `run_on_changed_to_ready` set. If it
is set and the event shows the MR entered ready state then trigger the webhook.
- `has_closing_action`: requires the webhook to have `run_on_closing` set. If it is and
the event shows the MR action is 'close' then trigger the webhook.

### GitlabNoteEvent Filters & Triggers

`GitalbNoteEvent` is used for any Gitlab event whose `object_kind` is `note`.

`GitlabNoteEvent` always includes these filters:

- `filter_note_type`: if the note event's noteable_type is not MergeRequest then ignore the event.

`GitlabNoteEvent` always includes these triggers:

- `has_requested_evaluation`: requires the webhook to have a non-empty
`request_evaluation_triggers` list. If it is not empty and the note text includes a matching
evaluation request then trigger the webhook. Each list item will be used to generate a string
`request-{item}-evaluation` and if the note event text startswith() that string then trigger
the webhook.
- `match_note_text`: requires the webhook to have a non-empty `note_text_patterns` list. If it is
not empty and the note text matches any of the `note_text_patterns` regexes then trigger the
webhook. This allows hooks to be triggered on whatever note text they like.

### JiraEvent Filters & Triggers

`JiraEvent` is used for any event whose header `message-type` is `jira`. `JiraEvent` does not have
specific filters or triggers beyond what BaseEvent provides.

### UmbBridgeEvent Filters & Triggers

`UmbBridgeEvent` is used for any event whose header `message-type` is `cki.kwf.umb-bz-event`.

`UmbBridgeEvent` has the following filters:

- `filter_target`: if the event's `event_target_webhook` header field value does not match
the current webhook then ignore the event.

`UmbBridgeEvent` does not have any triggers.

### AmqpEvent Filters & Triggers

`AmqpEvent` is used for any event whose header `message-type` is `amqp-bridge`.

`AmqpEvent` has the following filters:

- `filter_bug`: if the event is not associated with a BZ ID then ignore the event.

`AmqpEvent` does not have any triggers.
